package org.opencdmp.filetransformerbase.interfaces;

import com.sun.jdi.InvalidTypeException;
import org.opencdmp.commonmodels.models.FileEnvelopeModel;
import org.opencdmp.commonmodels.models.description.DescriptionModel;
import org.opencdmp.commonmodels.models.plan.PlanModel;
import org.opencdmp.filetransformerbase.models.misc.DescriptionImportModel;
import org.opencdmp.filetransformerbase.models.misc.PlanImportModel;
import org.opencdmp.filetransformerbase.models.misc.PreprocessingDescriptionModel;
import org.opencdmp.filetransformerbase.models.misc.PreprocessingPlanModel;

import javax.management.InvalidApplicationException;
import java.io.IOException;

/**
 * The FileTransformerClient interface represents the mechanism of depositing a plan to any
 * repository which mints a persistent digital object identifier (DOI) for each submission,
 * which makes the stored plans easily citeable.
 */
public interface FileTransformerClient {

    /**
     * Returns a string representing the persistent digital object identifier (DOI) which
     * was created.
     *
     * @param plan plan structure which is to be deposited
     * @return a string representing the persistent digital object identifier (DOI)
     * @throws Exception if an error occurs while trying to deposit the plan
     */
    FileEnvelopeModel exportPlan(PlanModel plan, String variant) throws InvalidApplicationException, IOException, InvalidTypeException;
    PlanModel importPlan(PlanImportModel planImportModel);

    FileEnvelopeModel exportDescription(DescriptionModel descriptionFileTransformerModel, String format) throws InvalidApplicationException, IOException;
    DescriptionModel importDescription(DescriptionImportModel descriptionImportModel);

    FileTransformerConfiguration getConfiguration();

    PreprocessingPlanModel preprocessingPlan(FileEnvelopeModel fileEnvelopeModel);

    PreprocessingDescriptionModel preprocessingDescription(FileEnvelopeModel fileEnvelopeModel);

}
