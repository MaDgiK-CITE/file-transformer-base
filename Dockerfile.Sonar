FROM maven:3-eclipse-temurin-21

RUN apt-get update \
    && apt-get install gpg -y \
    && apt-get clean

ARG MAVEN_ACCOUNT_USR
ARG MAVEN_ACCOUNT_PSW
ARG REVISION
ARG MAVEN_GPG_PASSPHRASE
ARG MAVEN_GPG_KEYNAME
ARG PROFILE
ARG DEV_PROFILE_URL_DEPOSIT
ARG DEV_PROFILE_URL
ARG ORACLE_URL
ARG ORACLE_TOKEN
ENV gpg_keyname=$MAVEN_GPG_KEYNAME
ENV gpg_passphrase=$MAVEN_GPG_PASSPHRASE
ENV server_username=$MAVEN_ACCOUNT_USR
ENV server_password=$MAVEN_ACCOUNT_PSW

COPY settings.xml /root/.m2/settings.xml
COPY keypair.asc /tmp/keypair.asc
RUN if [ "$PROFILE" = "ossrh" ]; then \
    gpg --batch --import /tmp/keypair.asc; \
  fi

COPY oracle.local.cite.gr.crt $JAVA_HOME/conf/security
RUN cd $JAVA_HOME/conf/security && keytool -cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias oraclecert -file oracle.local.cite.gr.crt

WORKDIR /build/

COPY . .

RUN mvn -Drevision=${REVISION} -DdevProfileUrlDeposit=${DEV_PROFILE_URL_DEPOSIT} -DdevProfileUrl=${DEV_PROFILE_URL} -P${PROFILE} clean install

RUN mvn sonar:sonar -Dsonar.projectKey=OpenDMP:file-transformer-base -Dsonar.login=${ORACLE_TOKEN} -Dsonar.host.url=${ORACLE_URL} -Dsonar.projectName='OpenDMP file-transformer-base'